﻿using OpenQA.Selenium;

namespace Page_Object_Model_Exercise.Page
{
    public interface IShoppingCartPage
    {
        IWebElement CartDeleteButton { get; }
        IWebElement CartEmptyAlert { get; }
        IWebElement ProductRow { get; }

        void WaitForCartAlert();
        void WaitForShoppingCartPage();
    }
}