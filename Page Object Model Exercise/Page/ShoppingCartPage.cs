﻿using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Support.UI;

namespace Page_Object_Model_Exercise.Page
{
    public class ShoppingCartPage : IShoppingCartPage
    {
        private IBasePage _basePage;
        public ShoppingCartPage(IBasePage basePage)
        {
            _basePage = basePage;
        }

        private IWebElement shoppingCartPage => _basePage.Driver.FindElement(By.Id("order"));
        public IWebElement ProductRow => shoppingCartPage.FindElement(By.Id("product_5_19_0_0"));
        public IWebElement CartDeleteButton => ProductRow.FindElement(By.CssSelector("td.cart_delete a"));
        private IWebElement centerColumn => shoppingCartPage.FindElement(By.Id("center_column"));

        public IWebElement CartEmptyAlert => centerColumn.FindElement(By.CssSelector("p"));


        public void WaitForCartAlert()
        {
            _basePage.Wait.Until(driver => CartEmptyAlert.Displayed);
            _basePage.Wait.Until(driver => CartEmptyAlert.Text == "Your shopping cart is empty.");
        }

        public void WaitForShoppingCartPage()
        {
            _basePage.Wait.Until(driver => shoppingCartPage.Displayed);
        }

    }
}
