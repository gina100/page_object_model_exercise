﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace Page_Object_Model_Exercise.Page
{
    public class BasePage : IBasePage
    {
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }
        public BasePage(IWebDriver driver)
        {
            Wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            this.Driver = driver;
        }
        public IWebElement PageRoot => Driver.FindElement(By.Id("page"));
    }
}
