﻿using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Support.UI;

namespace Page_Object_Model_Exercise.Page
{
    public class SearchPage : ISearchPage
    {
        private IBasePage _basePage;
        private IMainPage _mainPage;
        public SearchPage(IBasePage basePage,IMainPage mainPage)
        {
            _basePage = basePage;
            _mainPage = mainPage;
        }

        
        public IList<IWebElement> SearchedItemNames => _mainPage.CenterColumn.FindElements(By.CssSelector("a.product-name"));
        public IWebElement SearcheditemName => _mainPage.CenterColumn.FindElement(By.CssSelector("a.product-name"));


        public void WaitForResults()
        {
            _basePage.Wait.Until(driver => SearchedItemNames.Count > 0);
        }
    }
}
