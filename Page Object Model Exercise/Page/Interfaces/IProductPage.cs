﻿using OpenQA.Selenium;

namespace Page_Object_Model_Exercise.Page
{
    public interface IProductPage
    {
        IWebElement CartButton { get; }
        IWebElement CartForm { get; }
        IWebElement CartModal { get; }
        IWebElement CheckoutButton { get; }
        IWebElement EmailSentModal { get; }
        IWebElement FriendEmail { get; }
        IWebElement FriendName { get; }
        
        IWebElement SendEmailButton { get; }
        IWebElement SendFriendButton { get; }
        IWebElement SendEmailError { get; }

        void WaitForCartForm();
        void WaitForCartModal();
        void WaitForEmailForm();
        void WaitForEmailSentModal();
        void WaitForErrorMessage();
       
    }
}