﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Page_Object_Model_Exercise.Page
{
    public interface IBasePage
    {
        IWebElement PageRoot { get; }
        IWebDriver Driver { get; }
        WebDriverWait Wait { get; set; }
       
    }
}