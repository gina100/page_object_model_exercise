﻿using OpenQA.Selenium;

namespace Page_Object_Model_Exercise.Page
{
    public interface IMainPage
    {
        IWebElement SearchButton { get; }
        IWebElement SearchInput { get; }
        IWebElement CenterColumn { get; }
    }
}