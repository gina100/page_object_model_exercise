﻿using OpenQA.Selenium;

namespace Page_Object_Model_Exercise.Page
{
    public interface IStoreApp
    {
        IBasePage BasePage { get; set; }
        IMainPage MainPage { get; set; }
        IProductPage ProductPage { get; set; }
        ISearchPage SearchPage { get; set; }
        IShoppingCartPage ShoppingCartPage { get; set; }
    }
}