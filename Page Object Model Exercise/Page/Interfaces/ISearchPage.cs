﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace Page_Object_Model_Exercise.Page
{
    public interface ISearchPage
    {
        IList<IWebElement> SearchedItemNames { get; }
        IWebElement SearcheditemName { get; }

        void WaitForResults();
    }
}