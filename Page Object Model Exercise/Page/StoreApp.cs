﻿using OpenQA.Selenium;

namespace Page_Object_Model_Exercise.Page
{
    public class StoreApp : IStoreApp
    {
        public IBasePage BasePage { get; set; }
        public IMainPage MainPage { get; set; }
        public ISearchPage SearchPage { get; set; }
        public IProductPage ProductPage { get; set; }
        public IShoppingCartPage ShoppingCartPage { get; set; }

        public  StoreApp(IWebDriver driver)
        {
            driver.Navigate().GoToUrl("http://automationpractice.com/index.php");
            BasePage = new BasePage(driver);
            MainPage = new MainPage(BasePage);
            ShoppingCartPage = new ShoppingCartPage(BasePage);
            SearchPage = new SearchPage(BasePage,MainPage);
            ProductPage = new ProductPage(BasePage,MainPage);

        }
    }
}
