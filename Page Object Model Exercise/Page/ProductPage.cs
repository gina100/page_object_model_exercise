﻿using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Support.UI;

namespace Page_Object_Model_Exercise.Page
{
    public class ProductPage : IProductPage
    {
        private IBasePage _basePage;
        private IMainPage _mainPage;
        public ProductPage(IBasePage basePage, IMainPage mainPage)
        {
            _basePage = basePage;
            _mainPage = mainPage;
        }

        private IWebElement productPage => _basePage.Driver.FindElement(By.Id("product"));
        public IWebElement CartForm => _mainPage.CenterColumn.FindElement(By.CssSelector("div.box-cart-bottom"));
        public IWebElement CartButton => CartForm.FindElement(By.Name("Submit"));
        public IWebElement CartModal => productPage.FindElement(By.Id("layer_cart"));
        public IWebElement CheckoutButton=> CartModal.FindElement(By.CssSelector("a"));
        public IWebElement SendFriendButton => productPage.FindElement(By.Id("send_friend_button"));
        private IWebElement emailForm => productPage.FindElement(By.Id("send_friend_form_content"));
        public IWebElement FriendName => emailForm.FindElement(By.Id("friend_name"));
        public IWebElement FriendEmail => emailForm.FindElement(By.Id("friend_email"));
        public IWebElement SendEmailButton => productPage.FindElement(By.CssSelector("#sendEmail"));
        public IWebElement SendEmailError => productPage.FindElement(By.CssSelector("#send_friend_form_error"));

        public IWebElement EmailSentModal => productPage.FindElement(By.CssSelector("div.fancybox-wrap  p"));

        public void WaitForCartForm()
        {
            _basePage.Wait.Until(driver => CartForm.Displayed);
        }
        public void WaitForCartModal()
        {
            _basePage.Wait.Until(driver => CartModal.Displayed);
        }
        public void WaitForEmailForm()
        {
            _basePage.Wait.Until(driver => emailForm.Displayed);
        }
        public void WaitForEmailSentModal()
        {
            _basePage.Wait.Until(driver => EmailSentModal.Text == "Your e-mail has been sent successfully");
        }
        public void WaitForErrorMessage()
        {
            _basePage.Wait.Until(driver => SendEmailError.Text == "You did not fill required fields");
        }
        
    }
}
