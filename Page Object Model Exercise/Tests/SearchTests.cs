﻿using NUnit.Framework;
using Page_Object_Model_Exercise.Page;

namespace Page_Object_Model_Exercise.Tests
{
    [TestFixture]
    public class SearchTests 
    {
        private BaseTest _baseTest;
        [SetUp]
        public void Setup()
        {
            _baseTest = new BaseTest();
            _baseTest.Setup();
        }

        [Test]
        public void GivenAnExistingItemName_WhenSearching_ThenReturnItemCountGreaterThanZero()
        {
            _baseTest.StoreApp.MainPage.SearchInput.SendKeys("Dress");
            _baseTest.StoreApp.MainPage.SearchButton.Click();
            _baseTest.StoreApp.SearchPage.WaitForResults();
            Assert.IsTrue(_baseTest.StoreApp.SearchPage.SearchedItemNames.Count>0);
        }

        [TearDown]
        public void TearDown()
        {
            _baseTest.CleanUp();
        }
    }
}
