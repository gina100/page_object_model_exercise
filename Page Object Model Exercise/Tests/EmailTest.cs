﻿using NUnit.Framework;
using Page_Object_Model_Exercise.Page;

namespace Page_Object_Model_Exercise.Tests
{
    [TestFixture]
    public class EmailTest
    {
        private BaseTest _baseTest;
        [SetUp]
        public void Setup()
        {
            _baseTest = new BaseTest();
            _baseTest.Setup();
            _baseTest.StoreApp.MainPage.SearchInput.SendKeys("Dress");
            _baseTest.StoreApp.MainPage.SearchButton.Click();
            _baseTest.StoreApp.SearchPage.WaitForResults();
            _baseTest.StoreApp.SearchPage.SearcheditemName.Click();
            _baseTest.StoreApp.ProductPage.WaitForCartForm();
        }

        [Test]
        public void GivenValidInput_WhenSendingAnEmail_ThenDisplaySuccessModal()
        {
           
            _baseTest.StoreApp.ProductPage.SendFriendButton.Click();
            _baseTest.StoreApp.ProductPage.WaitForEmailForm();
            _baseTest.StoreApp.ProductPage.FriendName.SendKeys("K");
            _baseTest.StoreApp.ProductPage.FriendEmail.SendKeys("K@gmail.com");
            _baseTest.StoreApp.ProductPage.SendEmailButton.Click();
            _baseTest.StoreApp.ProductPage.WaitForEmailSentModal();

            Assert.IsTrue(_baseTest.StoreApp.ProductPage.EmailSentModal.Displayed);
        }

        [Test]
        public void GivenEmptyFields_WhenSendingAnEmail_ThenDisplayValidationMessage()
        {
            _baseTest.StoreApp.ProductPage.SendFriendButton.Click();
            _baseTest.StoreApp.ProductPage.WaitForEmailForm();
            _baseTest.StoreApp.ProductPage.SendEmailButton.Click();
            _baseTest.StoreApp.ProductPage.WaitForErrorMessage();

            Assert.IsTrue(_baseTest.StoreApp.ProductPage.SendEmailError.Displayed);
        }

        [TearDown]
        public void TearDown()
        {
            _baseTest.CleanUp();
        }
    }
}
