﻿using NUnit.Framework;
using Page_Object_Model_Exercise.Page;

namespace Page_Object_Model_Exercise.Tests
{
    [TestFixture]
    public class CartTest
    {
        private BaseTest _baseTest;
        [SetUp]
        public void Setup()
        {
            _baseTest = new BaseTest();
            _baseTest.Setup();
            _baseTest.StoreApp.MainPage.SearchInput.SendKeys("Dress");
            _baseTest.StoreApp.MainPage.SearchButton.Click();
            _baseTest.StoreApp.SearchPage.WaitForResults();
            _baseTest.StoreApp.SearchPage.SearcheditemName.Click();
        }

        [Test]
        public void GivenAnExistingItemName_WhenAddingToCart_ThenDisplayCartModal()
        {
            _baseTest.StoreApp.ProductPage.WaitForCartForm();
            _baseTest.StoreApp.ProductPage.CartButton.Click();
            _baseTest.StoreApp.ProductPage.WaitForCartModal();
            Assert.IsTrue(_baseTest.StoreApp.ProductPage.CartModal.Displayed);
        }

        [Test]
        public void GivenAnExistingCartItems_WhenRemovingcFromCart_ThenDisplayAlert()
        {
            _baseTest.StoreApp.ProductPage.WaitForCartForm();
            _baseTest.StoreApp.ProductPage.CartButton.Click();
            _baseTest.StoreApp.ProductPage.WaitForCartModal();
            _baseTest.StoreApp.ProductPage.CheckoutButton.Click();
            _baseTest.StoreApp.ShoppingCartPage.WaitForShoppingCartPage();
            _baseTest.StoreApp.ShoppingCartPage.CartDeleteButton.Click();
            _baseTest.StoreApp.ShoppingCartPage.WaitForCartAlert();

            Assert.IsTrue(_baseTest.StoreApp.ShoppingCartPage.CartEmptyAlert.Displayed);
        }

        [TearDown]
        public void TearDown()
        {
            _baseTest.CleanUp();
        }
    }
}
