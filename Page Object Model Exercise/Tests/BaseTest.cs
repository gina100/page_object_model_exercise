﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.IO;
using System.Reflection;
using Page_Object_Model_Exercise.Page;

namespace Page_Object_Model_Exercise.Tests
{
    [TestFixture]
    public class BaseTest
    {
        private IWebDriver driver;
        public IStoreApp StoreApp { get; set; }

        [SetUp]
        public void Setup()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("--start-maximized");
            driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), chromeOptions);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
            StoreApp=new StoreApp(driver);
        }


        [TearDown]
        public void CleanUp()
        {
            driver.Dispose();
        }
    }
}
